﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

[System.Serializable]   // This must be here to ensure that the secondary class is "serializable," meaning that Unity can view it


public class GC_Level_2 : LevelController
{
    // Function must be made a co-routine to use WaitForSeconds.Otherwise, the entire program would "wait" for seconds
    // co-routine returns this IEnumerator thing
    IEnumerator gameLoop()
    {
        yield return new WaitForSeconds(startWait);     // Wait at the beginning, outside the loop

        // Spawn the waves

        for (int wave = 0; wave < 5; wave++)
        {
            // Spawn one wave of enemies
            for (int i = 0; i < enemiesPerWave; i++)
            {
                // Random Range between the edges of the screen
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x),
                                                    spawnValues.y,
                                                    spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;     // This means no rotation

                int randomEnemyToSpawn = Random.Range(0, enemies.Length);

                // Create an enemy at the random spot
                Instantiate(enemies[randomEnemyToSpawn], spawnPosition, spawnRotation);

                // Wait until the next enemy spawns
                yield return new WaitForSeconds(spawnWait);
            }


            // Wait for the next wave to start
            yield return new WaitForSeconds(timeBetweenWaves);

            // Increase the amount of enemies on the next wave
            enemiesPerWave = enemiesPerWave * 1.3f;

            // Decrease the interval between spawns on the next wave
            spawnWait = spawnWait * 0.8f;

            SpawnPowerup();

            if (gameOver)
            {
                restartText.text = "Press 'R' to Restart";
                restart = true;
                break;
            }
        }

        yield return new WaitForSeconds(8);     // Wait at the end for the wave to finish

        // go to the next level
        SceneManager.LoadScene("Level3");
    }



    // Use this for initialization
    new void Start()
    {
        base.Start();

        //score = 0;                          // Score starts at zero

        UpdateScore();
        StartCoroutine(gameLoop());
    }


    new void Update()
    {
        base.Update();
    }
}