﻿using UnityEngine;
using System.Collections;

public class GC_LevelSelect : GameController
{

	// Use this for initialization
	new void Start ()
    {
        base.Start();

        score = 0;
    }

    public void startEndless()
    {
        // Load the next level
        UnityEngine.SceneManagement.SceneManager.LoadScene("Endless");
    }

    public void startBoss()
    {
        // Load the next level
        UnityEngine.SceneManagement.SceneManager.LoadScene("JerkOLantern");
    }

    // Update is called once per frame
    void Update()
    {
        // Poll for keypress of Space
        if (Input.GetKeyDown(KeyCode.S))
        {
            // Load the next level
            UnityEngine.SceneManagement.SceneManager.LoadScene("Level1");
        }

        // Poll for keypress of Space
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            // Load the next level
            UnityEngine.SceneManagement.SceneManager.LoadScene("Level1");
        }

        // Poll for keypress of Space
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            // Load the next level
            UnityEngine.SceneManagement.SceneManager.LoadScene("Level2");
        }

        // Poll for keypress of Space
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            // Load the next level
            UnityEngine.SceneManagement.SceneManager.LoadScene("Level3");
        }

        // Poll for keypress of Space
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            // Load the next level
            UnityEngine.SceneManagement.SceneManager.LoadScene("Level4");
        }

        // Poll for keypress of Space
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            // Load the next level
            UnityEngine.SceneManagement.SceneManager.LoadScene("Level5");
        }
    }
}
