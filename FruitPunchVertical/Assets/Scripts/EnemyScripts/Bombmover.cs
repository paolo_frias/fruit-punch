﻿using UnityEngine;
using System.Collections;

public class Bombmover : MonoBehaviour
{
    private Rigidbody2D bombBody;

    public float moveSpeed;

    public float frequency;

    public float magnitude;

    private Vector3 pos;

    // Use this for initialization
    void Start ()
    {
        pos = transform.position;
    }
	
	// Update is called once per frame
	void Update ()
    {
        pos += transform.up * Time.deltaTime * -moveSpeed;
        transform.position = pos + transform.right * Mathf.Sin(Time.time * frequency) * magnitude;
    }
}
