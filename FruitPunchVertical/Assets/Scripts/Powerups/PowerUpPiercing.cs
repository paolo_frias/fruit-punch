﻿using UnityEngine;
using System.Collections;

public class PowerUpPiercing : PowerupMover
{
    // Called when something collides with this object. Used for the powerUp getting picked up by the player
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            thePlayer.powerUpPiercing();
            Destroy(gameObject);        // The powerUp goes away
        }
    }
}
