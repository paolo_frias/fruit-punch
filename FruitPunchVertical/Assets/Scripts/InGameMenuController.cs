﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class InGameMenuController : GameController {

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape)) {
			if (gamePaused)
				ResumeGame();
			else if (!gamePaused) {
				restartText.text = "Press 'L' to Go To Level Selector";
				PauseGame();
			}
		}

		if (gamePaused && Input.GetKeyDown(KeyCode.R)) {
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
			ResumeGame(); //or else loaded scene will stay paused
		}

		if (gamePaused && Input.GetKeyDown(KeyCode.L)) {
			SceneManager.LoadScene("LevelSelect");
			ResumeGame(); //or else loaded scene will stay paused
		}

		if (gamePaused && Input.GetKeyDown(KeyCode.Q)) {
			Application.Quit();
		}

		if (gameOver) {
			restartText.text = "Press 'L' to Go To Level Selector";

			if (Input.GetKeyDown(KeyCode.L)) {
				SceneManager.LoadScene("LevelSelect");
			}
		}
	}
}
