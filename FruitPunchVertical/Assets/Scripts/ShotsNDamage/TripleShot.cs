﻿using UnityEngine;
using System.Collections;

public class TripleShot : ShotMover
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemy")
        {
            Destroy(gameObject);
        }
    }
}
