﻿using UnityEngine;
using System.Collections;

[System.Serializable]   // This must be here to ensure that the secondary class is "serializable," meaning that Unity can view it
public class ScreenEdge
{
    // public values to represent the edges of the gameboard.
    public float xMin, xMax, yMin, yMax;

}

public class GameController : MonoBehaviour
{
    public ScreenEdge theEdges;                 // Edges of the screen

    public AudioClip[] backgroundMusic;         // The list of background songs

    protected AudioSource musicPlayer;          // The player for the background music
    protected bool gameOver;                    // Whether or not the game is over
    static protected int score;                        // The current score
    public GameObject gameOverSprite;           // The sprite to display when the player loses
    public TextMesh scoreText;                   // The text to display the current score
    public GUIText restartText;                 // The text to prompt for a restart

    protected bool restart;                     // Whether to restart the game

    protected bool gamePaused;                  // Whether or not the game is currently paused
    public GameObject pauseScreen;              // handle to the pause screen for working



    protected void Start()
    {
        musicPlayer = GetComponent<AudioSource>();
        musicPlayer.clip = backgroundMusic[Random.Range(0, backgroundMusic.Length)];
        musicPlayer.Play();
        gameOver = false;                   // The game isn't over, it's starting
        restart = false;                    // We're not restarting. We're starting
        restartText.text = "";              // Clear the restart text
        gameOverSprite.SetActive(false);    // The game hasn't started yet
        scoreText.text = "";                // Clear the score text
    }

    // public function to be called whenever something is destroyed.
    // Takes in the amount of points to award for the destruction.
    public void AddScore(int pointsOfDestroyedEnemy)
    {
        score += pointsOfDestroyedEnemy;
        UpdateScore();
    }


    // Function to be called when the game ends (the player dies).
    public void GameOver()
    {
        gameOverSprite.SetActive(true);
        gameOver = true;
    }


    // function to update the score display on the screen.
    protected void UpdateScore()
    {
        scoreText.text = "" + score;
    }


    protected void PauseGame()
    {
        Time.timeScale = 0;
        gamePaused = true;
        pauseScreen.SetActive(true);
    }


    protected void ResumeGame()
    {
        Time.timeScale = 1;
        gamePaused = false;
        pauseScreen.SetActive(false);
    }
}
