﻿using UnityEngine;
using System.Collections;

public class AttackFinished : MonoBehaviour
{

    public GameObject theBoss;

    public void attackDone()
    {
        theBoss.GetComponent<JoL_Controller>().JoLattackFinished();
    }
}
