﻿using UnityEngine;
using System.Collections;

public class EnemyMover : MonoBehaviour
{
    private Rigidbody2D baddieBody;

    public float moveSpeed;

	// Use this for initialization
	void Start ()
    {
        baddieBody = GetComponent<Rigidbody2D>();
        baddieBody.velocity = transform.up * -moveSpeed;
	}
}
