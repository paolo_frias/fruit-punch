﻿using UnityEngine;
using System.Collections;

public class Headspin : MonoBehaviour
{
    private float timeElapsed;

    public float rotationSpeed;

    // Update is called once per frame
    void Update ()
    {
        timeElapsed = timeElapsed + Time.deltaTime;                 // update time elapsed for rotation
        transform.localRotation = Quaternion.Euler(new Vector3(0, 0, timeElapsed * rotationSpeed));
    }
}
