﻿using UnityEngine;
using System.Collections;

public class ShotMover : DamageDealer
{
    private Rigidbody2D shotBody;

    public float speed;

	// Use this for initialization
	void Start ()
    {
        shotBody = GetComponent<Rigidbody2D>();
        shotBody.velocity = transform.up * speed;
	}
}
