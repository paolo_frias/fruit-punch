﻿using UnityEngine;
using System.Collections;

public class FlashOnDamage : MonoBehaviour
{

    private SpriteRenderer theSprite;

    IEnumerator flashDone()
    {
        yield return new WaitForSeconds(0.05f);

        theSprite.color = Color.white;
    }

    // Use this for initialization
    void Start()
    {
        theSprite = GetComponent<SpriteRenderer>();
    }

    // called when "other" collides with this object. "other" is
    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.tag == "DamageDealer")
        {
            theSprite.color = new Color(.8f, .8f, .8f, 1);

            StartCoroutine(flashDone());
        }

    }
}
