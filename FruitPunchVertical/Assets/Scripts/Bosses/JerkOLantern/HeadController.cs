﻿using UnityEngine;
using System.Collections;

public class HeadController : MonoBehaviour
{
    public ScreenEdge bounceBorder;            // Edges of the screen

    private Rigidbody2D headBody;

	public GameObject headThrower;

	private bool headLaunched;

    public float moveSpeed;

	Vector3 startLocation;
	Quaternion startRotation;


    void Awake()
    {
        headBody = GetComponent<Rigidbody2D>();
        startLocation = transform.localPosition;
        startRotation = transform.localRotation;
        headLaunched = false;
    }

    // Use this for initialization
    void Start()
    {
		//startLocation = transform.localPosition;
		//startRotation = transform.localRotation;
        //headLaunched = false;

		//throwHeadStart ();
    }

	public void throwHeadStart()
	{
		headLaunched = false;
        transform.localPosition = startLocation;
        transform.localRotation = startRotation;
        int[] initialSpeed = { -1, 1 };
		headBody.velocity = new Vector3(initialSpeed[Random.Range(0, initialSpeed.Length)], -1, 0) * moveSpeed;
	}


	// Called to put the head back where it's supposed to be
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "HeadLocation" && headLaunched == true) 
		{
			headThrower.GetComponent<AttackFinished> ().attackDone ();
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag == "HeadLocation")
			headLaunched = true;
	}


    // called every "physics step"
    void FixedUpdate ()
    {
        if (headBody.position.x < bounceBorder.xMin)
        {
            Vector3 temp = headBody.velocity;
            temp.x = -temp.x;
            headBody.velocity = temp;
            //headBody.position = new Vector2(bounceBorder.xMin + .01f, headBody.position.y);
        }

        else if (headBody.position.x > bounceBorder.xMax)
        {
            Vector3 temp = headBody.velocity;
            temp.x = -temp.x;
            headBody.velocity = temp;
            //headBody.position = new Vector2(bounceBorder.xMax - .01f, headBody.position.y);
        }

        if (headBody.position.y < bounceBorder.yMin)
        {
            Vector3 temp = headBody.velocity;
            temp.y = -temp.y;
            headBody.velocity = temp;
            //headBody.position = new Vector2(headBody.position.x, bounceBorder.yMin + .01f);
        }

        else if (headBody.position.y > bounceBorder.yMax)
        {
            Vector3 temp = headBody.velocity;
            temp.y = -temp.y;
            headBody.velocity = temp;
            //headBody.position = new Vector2(headBody.position.x, bounceBorder.yMax - .01f);
        }
    }
}
