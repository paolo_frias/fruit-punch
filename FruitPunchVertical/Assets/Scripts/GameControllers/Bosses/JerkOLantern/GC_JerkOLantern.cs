﻿using UnityEngine;
using System.Collections;

public class GC_JerkOLantern : GameController
{

    public GameObject levelIntro;               // handle to the level intro sprite

    // Use this for initialization
    new void Start ()
    {
        base.Start();
        UpdateScore();

        StartCoroutine(playIntro());
    }

    IEnumerator playIntro()
    {
        levelIntro.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        levelIntro.SetActive(false);
    }



    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape))
        {
            if (gamePaused)
                ResumeGame();
            else if (!gamePaused)
            {
                restartText.text = "Press 'L' to Go To Level Selector";
                PauseGame();
            }
        }

        if (gamePaused && Input.GetKeyDown(KeyCode.R))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
            ResumeGame(); //or else loaded scene will stay paused
        }

        if (gamePaused && Input.GetKeyDown(KeyCode.L))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("LevelSelect");
            ResumeGame(); //or else loaded scene will stay paused
        }

		if (gamePaused && Input.GetKeyDown(KeyCode.T))
		{
			UnityEngine.SceneManagement.SceneManager.LoadScene("TitleScreen");
			ResumeGame(); //or else loaded scene will stay paused
		}

        if (gamePaused && Input.GetKeyDown(KeyCode.Q))
        {
            Application.Quit();
        }

        if (gameOver)
        {
            restartText.text = "Press 'L' to Go To Level Selector";

            if (Input.GetKeyDown(KeyCode.L))
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene("LevelSelect");
            }
        }
    }
}
