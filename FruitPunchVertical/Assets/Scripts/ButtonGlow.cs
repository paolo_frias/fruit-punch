﻿using UnityEngine;
using System.Collections;

public class ButtonGlow : MonoBehaviour 
{
	public float glowAmount;                               // The amount of color to shift
	public float glowPeriod;                                // The time to spend changing color

	private float timeElapsed;
	private SpriteRenderer theSprite;


	// Use this for initialization
	void Start ()
	{
		theSprite = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		timeElapsed = timeElapsed + Time.deltaTime;                 	// update time elapsed for rotation
		float interval = Mathf.Abs(Mathf.Sin(timeElapsed / glowPeriod));   		// Sine function for smooth transition between -1 and 1
		theSprite.color = new Color(interval, interval, interval, 1);

	}
}
