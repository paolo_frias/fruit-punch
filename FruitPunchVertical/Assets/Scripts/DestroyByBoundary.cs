﻿using UnityEngine;
using System.Collections;

public class DestroyByBoundary : MonoBehaviour
{
    // Destroy any and all 2D colliders that exit the box
    void OnTriggerExit2D(Collider2D other)
    {
        Destroy(other.gameObject);
    }
}
