﻿using UnityEngine;
using System.Collections;

[System.Serializable]   // This must be here to ensure that the secondary class is "serializable," meaning that Unity can view it
						// We make a secondary class here to separate out the values on the Inspector in Unity. For tidyness.
public struct Boundary {
	// public values to represent the edges of the gameboard.
	public float xMin, xMax, yMin, yMax;

}

[System.Serializable]
public struct Bullets {
	// handles to the various bullets that coincide with the powerups
	public GameObject standard, piercing, triple;
}

public class PlayerController : MonoBehaviour {
	private GameController gameController;  // Handle to the Game Controller in unity

	public Boundary theBoundaries;          // The boundary for restricting movement
	public GameObject playerDeath;
	public Bullets theBullets;              // The shots to fire



	private Rigidbody2D playerBody;         // Variable to hold reference to the Rigidbody for movement
	private SpriteRenderer playerSpriteRenderer;    // Variable to hold reference to Sprite Renderer for changing sprite when firing
	private enum SpriteIndex { Normal, FireRate, Piercing, Triple }
	public Sprite[] playerSprites;
    public Sprite[] ammoCountSprites;
    public GameObject ammoDisplay;
    private SpriteRenderer ammoSpriteRenderer;
	private int currSpriteIndex;

	public float moveSpeed;                 // Player movement speed

	public Transform shotSpawn;             // Location within the world that shots will spawn at

	public int maxHealthPoints;
	public int healthPoints;
	public float invulnerableLength;
	private float timeOfDamageTaken;
	private float nextDamageTaken;
	private bool invulnerable;
	private float invulTimer;

	public bool playerDashing;
	public int dashDistance;
	public float dashCooldown;
	public float nextDash;
	public float timeOfDash;

	public const float rateOfFire = .25f;
	private float fireRate;                 // The time in seconds between shots
	private float nextFire;                 // The time at which we can fire again
	private bool tripleShot;                // whether the player has the tripleshot powerup
	public bool piercingShot;               // whether the player has piercing

	public bool powerUp;
	public float powerUpDuration;           // The time in seconds that a powerup lasts for
	private float powerUpTimeout;           // The time at which the powerUp goes away

	public float shootAnimationTime;        // The amount of time to hold the firing sprite
	private float switchBackTime;           // When to switch back to the standard sprite

    public int ammoPerClip = 16;     // The amount of ammo in one clip  
    private int currentAmmo;                // The amount of ammo left in the clip

    private AudioSource reloadPlayer;       // The player for the gun reloading
    public AudioClip reloadSound;          // the reloading sound effect



    // Initialize values and retrieve reference components
    void Start() {
		// Search for an instance of a Game Object tagged "GameController"
		GameObject gameControllerObject = GameObject.FindWithTag("GameController");

		// if we found one...
		if (gameControllerObject != null) {
			// assign our GameController handle to the instance that we found
			gameController = gameControllerObject.GetComponent<GameController>();
		} else
			Debug.Log("No GameController found in scene!");



		playerBody = GetComponent<Rigidbody2D>();
		playerSpriteRenderer = GetComponent<SpriteRenderer>();
        ammoSpriteRenderer = ammoDisplay.GetComponent<SpriteRenderer>();
		currSpriteIndex = (int)SpriteIndex.Normal;
		playerSpriteRenderer.sprite = playerSprites[currSpriteIndex];
		invulnerable = false;
		nextDamageTaken = Time.time;
		nextDash = Time.time;
		fireRate = rateOfFire;
		nextFire = 0.0f;
		powerUpTimeout = 0.0f;
		tripleShot = false;
		powerUp = false;

        reloadPlayer = GetComponent<AudioSource>();
        currentAmmo = ammoPerClip;
        reloadPlayer.clip = reloadSound;
        ammoSpriteRenderer.sprite = ammoCountSprites[currentAmmo];
    }


	// called every frame
	void Update() {

		if (Time.time > powerUpTimeout && powerUp) {
			powerUpOver();
			powerUp = false;
		}

		// Polls for keypress of "Fire1" key, and checks to make sure the current time is past the next fire time
		if ((Input.GetMouseButton(0) || Input.GetKey(KeyCode.Space)) && Time.time > nextFire && currentAmmo > 0)
        {
			// updates the time when we can fire next
			nextFire = Time.time + fireRate;

			// Updates the time at which to toggle back to the standard sprite
			switchBackTime = Time.time + shootAnimationTime;
			
			if (tripleShot)
				shootTriple();
			else if (piercingShot)
				shootPiercing();
			else
				shootNormal();

            currentAmmo--;
            ammoSpriteRenderer.sprite = ammoCountSprites[currentAmmo];
		}


        if (Input.GetMouseButtonDown(1) || Input.GetKeyDown(KeyCode.R))
        {
            Reload();
        }


		//Dashing
		if ((Input.GetKeyDown(KeyCode.Q) ||
			(Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.LeftShift)))
			&& Time.time > nextDash) {
			//left dashing
			StartCoroutine(Dash(-dashDistance));
			nextDash = Time.time + dashCooldown;
		}

		if ((Input.GetKeyDown(KeyCode.E) ||
			(Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.LeftShift)))
			&& Time.time > nextDash) {
			//right dashing
			StartCoroutine(Dash(dashDistance));
			nextDash = Time.time + dashCooldown;
		}


		//Show Invulnerability
		if(invulnerable) {
			invulTimer = invulTimer + Time.deltaTime;                     // update time elapsed for r)otation
			float interval = Mathf.Abs(Mathf.Sin(20 * invulTimer / invulnerableLength));        // Sine function for smooth transition between -1 and 1
			//Debug.Log(interval);
			playerSpriteRenderer.color = new Color(playerSpriteRenderer.color.r, playerSpriteRenderer.color.g, playerSpriteRenderer.color.b, interval);
		}
	}

    private void Reload()
    {
        currentAmmo = ammoPerClip;
        nextFire = Time.time + (fireRate * 3);
        reloadPlayer.Play();
        ammoSpriteRenderer.sprite = ammoCountSprites[currentAmmo];
    }

	private IEnumerator Dash(float dist) {
		timeOfDash = Time.time;
		playerDashing = true;
		if (dist > 0)
			playerSpriteRenderer.sprite = playerSprites[(currSpriteIndex * 3) + 2];
		else
			playerSpriteRenderer.sprite = playerSprites[(currSpriteIndex * 3) + 1];
		for (int i = 0; i < 20; i++) {
			playerBody.position = new Vector2(playerBody.position.x + dist / 10, playerBody.position.y);
			yield return null;
		}
		playerSpriteRenderer.sprite = playerSprites[currSpriteIndex * 3];
		playerDashing = false;
	}


	private void shootNormal() {
		// Instantiate() literally instantiates a GameObject.
		//      Takes in what to instantiate (GameObject),
		//      the Position to make it at (Vector3),
		//      and the rotation to make it with (Quaternian (wtf))
		Instantiate(theBullets.standard, shotSpawn.position, shotSpawn.rotation);
	}

	private void shootPiercing() {
		Instantiate(theBullets.piercing, shotSpawn.position, shotSpawn.rotation);
	}


	private void shootTriple() {
		Instantiate(theBullets.triple, shotSpawn.position, shotSpawn.rotation);
		Instantiate(theBullets.triple, shotSpawn.position, shotSpawn.rotation * Quaternion.Euler(new Vector3(0, 0, 15)));
		Instantiate(theBullets.triple, shotSpawn.position, shotSpawn.rotation * Quaternion.Euler(new Vector3(0, 0, -15)));

	}


	public void powerUpFireRate() {
		powerUpTimeout = Time.time + powerUpDuration;
		fireRate = fireRate / 3;
		currSpriteIndex = (int)SpriteIndex.FireRate;
		playerSpriteRenderer.sprite = playerSprites[currSpriteIndex * 3];
		powerUp = true;
	}

	public void powerUpPiercing() {
		powerUpTimeout = Time.time + powerUpDuration;
		currSpriteIndex = (int)SpriteIndex.Piercing;
		playerSpriteRenderer.sprite = playerSprites[currSpriteIndex * 3];
		piercingShot = true;
		powerUp = true;
	}

	public void powerUpTripleshot() {
		powerUpTimeout = Time.time + powerUpDuration;
		tripleShot = true;
		powerUp = true;
		currSpriteIndex = (int)SpriteIndex.Triple;
		playerSpriteRenderer.sprite = playerSprites[currSpriteIndex * 3];
	}

	private void powerUpOver() {
		fireRate = rateOfFire;
		tripleShot = false;
		piercingShot = false;
		powerUp = false;
		currSpriteIndex = (int)SpriteIndex.Normal;

		playerSpriteRenderer.sprite = playerSprites[currSpriteIndex * 3];
	}


	// Called once every "physics step"
	void FixedUpdate() {
		// Get input from player
		int W = Input.GetKey(KeyCode.W) ? 1 : 0;
		int A = Input.GetKey(KeyCode.A) ? 1 : 0;
		int S = Input.GetKey(KeyCode.S) ? 1 : 0;
		int D = Input.GetKey(KeyCode.D) ? 1 : 0;

		float moveHorizontal = D - A;
		float moveVertical = W - S;

		Vector2 movementVector = new Vector2(moveHorizontal, moveVertical);


		playerBody.velocity = movementVector * moveSpeed;


		// Constrain the player to the board
		playerBody.position = new Vector2
			(
				Mathf.Clamp(playerBody.position.x, theBoundaries.xMin, theBoundaries.xMax),
				Mathf.Clamp(playerBody.position.y, theBoundaries.yMin, theBoundaries.yMax)
			);
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Enemy") {
			if (Time.time > nextDamageTaken) {
				EnemyCollisionDestruction ecd = other.GetComponentInParent<EnemyCollisionDestruction>();
				if(ecd.damageDealt > 0) {
					timeOfDamageTaken = Time.time;
					nextDamageTaken = Time.time + invulnerableLength;
					TakeDamage(ecd.damageDealt);
				}
			}
		}

		if(other.tag == "DamageDealer") {
			DamageDealer dd = other.GetComponent<DamageDealer>();
			if(dd.damageDealt > 0) {
				TakeDamage(dd.damageDealt);
			}
		}

        if(other.tag == "JerkOLantern") {
            EnemyCollisionDestruction ecdBoss = other.GetComponentInParent<EnemyCollisionDestruction>();
            if(ecdBoss.damageDealt > 0) {
                //maybe don't have damage cooldown for this?
                TakeDamage(ecdBoss.damageDealt);
            }
        }

	}

	void OnTriggerStay2D(Collider2D other) {
		if (other.tag == "Enemy") {
			if (Time.time > nextDamageTaken) {
				EnemyCollisionDestruction ecd = other.GetComponent<EnemyCollisionDestruction>();
				if (ecd.damageDealt > 0) {
					timeOfDamageTaken = Time.time;
					nextDamageTaken = Time.time + invulnerableLength;
					TakeDamage(ecd.damageDealt);
				}
			}

		}
	}


	void TakeDamage(int damage)
    {
		//Debug.Log("OWWIE");
		healthPoints -= damage;
		invulTimer = Time.time;
		StartCoroutine(Invulnerability());
		if (healthPoints <= 0)
			Die();
	}

	public bool Die() {
		Instantiate(playerDeath, transform.position, transform.rotation);
		gameController.GameOver();
		Destroy(gameObject);
		return true;
	}

	private IEnumerator Invulnerability() {
		Color original = playerSpriteRenderer.color;
		invulnerable = true;
		yield return new WaitForSeconds(invulnerableLength);
		invulnerable = false;
		playerSpriteRenderer.color = original;
	}
}
