﻿using UnityEngine;
using System.Collections;

public class CutsceneOver : MonoBehaviour
{
    public void cutsceneEnd()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("LeveLSelect");
    }
}
