﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelController : GameController
{

    public GameObject[] enemies;                // handle to the enemies to spawn
    public GameObject[] powerups;               // handle to the powerup to spawn

    public GameObject levelIntro;               // handle to the level intro sprite

    public float enemiesPerWave;                // Number of enemies in one wave
    public float spawnWait;                     // Time to wait between each individual spawn
    public float timeBetweenWaves;              // Time to wait between each wave

    public Vector3 spawnValues;                 // Where the enemies can spawn. Represents the edges of the screen at the top

    public float startWait;                     // Time to wait at the beginning of the game



    new protected void Start()
    {
        base.Start();
        StartCoroutine(playIntro());
    }

    IEnumerator playIntro()
    {
        levelIntro.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        levelIntro.SetActive(false);
    }

    // function to spawn a power up on the screen
    protected void SpawnPowerup()
    {
        Vector3 spawnPosition = new Vector3(Random.Range(theEdges.xMin, theEdges.xMax),
                                                Random.Range(theEdges.yMin, theEdges.yMax),
                                                0);

        Quaternion spawnRotation = Quaternion.identity;     // This means no rotation

        int randomPowerUpToSpawn = Random.Range(0, powerups.Length);


        // Create a powerup
        Instantiate(powerups[randomPowerUpToSpawn], spawnPosition, spawnRotation);
    }


    protected void Update()
    {
        if (Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape))
        {
            if (gamePaused)
                ResumeGame();
            else if (!gamePaused)
            {
                restartText.text = "Press 'L' to Go To Level Selector";
                PauseGame();
            }
        }

        if (gamePaused && Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            ResumeGame(); //or else loaded scene will stay paused
        }

        if (gamePaused && Input.GetKeyDown(KeyCode.L))
        {
            SceneManager.LoadScene("LevelSelect");
            ResumeGame(); //or else loaded scene will stay paused
        }

        if (gamePaused && Input.GetKeyDown(KeyCode.T))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("TitleScreen");
            ResumeGame(); //or else loaded scene will stay paused
        }

        if (gamePaused && Input.GetKeyDown(KeyCode.Q))
        {
            Application.Quit();
        }

        if (gameOver)
        {
            restartText.text = "Press 'L' to Go To Level Selector";

            if (Input.GetKeyDown(KeyCode.L))
            {
                SceneManager.LoadScene("LevelSelect");
            }
        }
    }
}
