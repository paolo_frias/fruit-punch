﻿using UnityEngine;
using System.Collections;

public class JoL_Controller : MonoBehaviour
{
    private Rigidbody2D lanternBody;

    public ScreenEdge bossBoundaries;

    //list of the different actions the lantern can do
    public GameObject obj_moveLeft;
    public GameObject obj_moveRight;
    public GameObject obj_stabLeft;
    public GameObject obj_stabRight;
    public GameObject obj_sweepLeft;
    public GameObject obj_sweepRight;
    public GameObject obj_throwHeadLeft;
    public GameObject obj_throwHeadRight;
	public GameObject obj_headLeft;
	public GameObject obj_headRight;

    public AudioClip[] lanternSoundEffects;
    private AudioSource laughPlayer;        // The player for the JerkO's laughs



    private bool movingLeft;                // boolean to tell which direction we're currently going

    public float moveSpeed;                 // public variable for the lantern's movement speed

    private float attackPosition;           // The X coordinate of the attack
	private float throwPosition;			// the X coordinate of the throw

    private bool hasAttacked;
    private bool hasThrownHead;

    // Use this for initialization
    void Start()
    {
        lanternBody = GetComponent<Rigidbody2D>();

        laughPlayer = GetComponent<AudioSource>();
        laughPlayer.clip = lanternSoundEffects[0];
        laughPlayer.Play();

        deactivateAll();

        movingLeft = true;
        hasAttacked = true;
        hasThrownHead = true;

        StartCoroutine(startJerkRun());
    }

    IEnumerator startJerkRun()
    {
        yield return new WaitForSeconds(8);

        runLeft();
    }


    // Update is called once per frame
    void Update ()
    {
        if (movingLeft)
        {
            if (lanternBody.position.x < attackPosition && !hasAttacked)
                attack();

			if (lanternBody.position.x < throwPosition && !hasThrownHead) 
			{
				int throwBool = Random.Range(0, 2);
				hasThrownHead = true;
				if (throwBool == 1)
					throwHead ();
			}
        }


        else
        {
            if (lanternBody.position.x > attackPosition && !hasAttacked)
                attack();

			if (lanternBody.position.x > throwPosition && !hasThrownHead) 
			{
				int throwBool = Random.Range(0, 2);
				hasThrownHead = true;
				if (throwBool == 1) 
					throwHead ();
			}
        }


        if (lanternBody.position.x < bossBoundaries.xMin)
            passRight();

        if (lanternBody.position.x > bossBoundaries.xMax)
            passLeft();

	}


    private void passLeft()
    {
        movingLeft = true;
        hasAttacked = false;
		hasThrownHead = false;
        runLeft();
        attackPosition = Random.Range(-20.0f, 20.0f);
		throwPosition = Random.Range (-20.0f, 20.0f);
		transform.position = new Vector3 (transform.position.x, Random.Range(-10.0f, 14.0f), transform.position.z);

        int laughBool = Random.Range(0, 2);
        laughPlayer.clip = lanternSoundEffects[1];
        if (laughBool == 1)
            laughPlayer.Play();
    }

    private void passRight()
    {
        movingLeft = false;
        hasAttacked = false;
		hasThrownHead = false;
        runRight();
        attackPosition = Random.Range(-20.0f, 20.0f);
		throwPosition = Random.Range (-20.0f, 20.0f);
		transform.position = new Vector3 (transform.position.x, Random.Range(-10.0f, 14.0f), transform.position.z);

        int laughBool = Random.Range(0, 2);
        laughPlayer.clip = lanternSoundEffects[1];
        if (laughBool == 1)
            laughPlayer.Play();
    }



    // ---------------------------------CLEAR STATE--------------------------------------

    // function to clear state
    private void deactivateAll()
    {
        obj_moveLeft.SetActive(false);
        obj_moveRight.SetActive(false);
        obj_stabLeft.SetActive(false);
        obj_stabRight.SetActive(false);
        obj_sweepLeft.SetActive(false);
        obj_sweepRight.SetActive(false);
		obj_throwHeadLeft.SetActive(false);
		obj_throwHeadRight.SetActive(false);
    }

    // --------------------------------------MOVEMENT--------------------------------------

    // function to move the Jerk left
    private void runLeft()
    {
        deactivateAll();
        obj_moveLeft.SetActive(true);
        lanternBody.velocity = transform.right * -moveSpeed;
    }

    // function to move the Jerk right
    private void runRight()
    {
        deactivateAll();
        obj_moveRight.SetActive(true);
        lanternBody.velocity = transform.right * moveSpeed;
    }

    // ------------------------------------STAB ATTACKS--------------------------------------

    // function for a stab attack when moving left
    private void stabLeft()
    {
        lanternBody.velocity = transform.right * 0;
        deactivateAll();

        obj_stabLeft.SetActive(true);
    }

    // function for a stab attack when moving right
    private void stabRight()
    {
        lanternBody.velocity = transform.right * 0;
        deactivateAll();

        obj_stabRight.SetActive(true);
    }

    // function to execute a stab attack
    private void stabAttack()
    {
        if (movingLeft)
        {
            stabLeft();
        }

        else
        {
            stabRight();
        }
    }

    // ---------------------------------SWEEP ATTACKS--------------------------------------

    // function to execute a sweeping attack
    private void sweepAttack()
    {
        if (movingLeft)
        {
            sweepLeft();
        }

        else
        {
            sweepRight();
        }
    }

    
    private void sweepLeft()
    {
        lanternBody.velocity = transform.right * 0;
        deactivateAll();

        obj_sweepLeft.SetActive(true);
    }
    
    
    private void sweepRight()
    {
        lanternBody.velocity = transform.right * 0;
        deactivateAll();

        obj_sweepRight.SetActive(true);
    }

    // ---------------------------------HEAD THROWS--------------------------------------

    private void throwHead()
    {
        if (movingLeft)
            throwHeadLeft();

        else
            throwHeadRight();
    }

    private void throwHeadLeft()
    {
        lanternBody.velocity = transform.right * 0;
        deactivateAll();

        obj_throwHeadLeft.SetActive(true);
		obj_headLeft.GetComponent<HeadController> ().throwHeadStart ();
    }

    private void throwHeadRight()
    {
        lanternBody.velocity = transform.right * 0;
        deactivateAll();

        obj_throwHeadRight.SetActive(true);
		obj_headRight.GetComponent<HeadController> ().throwHeadStart ();
    }


    // ---------------------------------ATTACK HANDLING--------------------------------------


    private void attack()
    {
        hasAttacked = true;
        float attackToDo = Random.Range(-15.0f, 14.0f);

		if (attackToDo < transform.position.y)
			stabAttack ();
		else
			sweepAttack ();
    }

    public void JoLattackFinished()
    {
        if (movingLeft)
            runLeft();

        else
            runRight();
    }
}
