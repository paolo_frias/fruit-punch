﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDScript : MonoBehaviour {

	public GameObject player;
	private PlayerController pc;
	public int playerMaxHealth;
	private int playerHealth;
	public Image[] heartImages;
	public Image hitNotifier;
	public float hitNotifyLength;


	public Image dashCooldownBar;
	// Use this for initialization
	void Start() {
		pc = player.GetComponent<PlayerController>();
		playerHealth = pc.maxHealthPoints;
		playerMaxHealth = pc.maxHealthPoints;
	}

	// Update is called once per frame
	void Update() {
		if (pc.healthPoints < playerHealth) {
			//player took damage
			StartCoroutine("HitNotification");
			Debug.Log("HUB change health");
			playerHealth = pc.healthPoints;
			UpdateHealth();
		}

		if (!pc) {
			playerHealth = 0;
			UpdateHealth();
			Destroy(this);
		}

		if(pc.nextDash > Time.time) {
			dashCooldownBar.fillAmount = (Time.time - pc.timeOfDash) / pc.dashCooldown;
		} else {
			dashCooldownBar.fillAmount = 1;
		}
	}

	void UpdateHealth() {
		if (playerHealth < 0) playerHealth = 0;
		for (int i = playerHealth; i < playerMaxHealth; i++) {
			heartImages[i].color = new Color(1, 1, 1, .15f);
		}
	}

	IEnumerator HitNotification() {
		hitNotifier.color = new Color(1, .1f, .1f, .6f);
		yield return new WaitForSeconds(hitNotifyLength);
		hitNotifier.color = new Color(0, 0, 0, 0);
		yield return null;
	}
}

