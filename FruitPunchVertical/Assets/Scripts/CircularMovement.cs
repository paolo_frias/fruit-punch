﻿using UnityEngine;
using System.Collections;

public class CircularMovement : MonoBehaviour {

	public GameObject playerObject;
	private PlayerController pc;
	private Vector3 v;
	public float degreesPerSecond;
	// Use this for initialization
	void Start () {
		pc = playerObject.GetComponent<PlayerController>();
		v = transform.position - pc.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		v = Quaternion.AngleAxis(degreesPerSecond * Time.deltaTime, Vector3.forward) * v;
		transform.position = pc.transform.position + v;
	}

	/*void OnTriggerEnter2D(Collider2D other) {
		if(other.tag == "Enemy") {
			EnemyCollisionDestruction edc = other.gameObject.GetComponent<EnemyCollisionDestruction>();
			edc.
		}
	}*/
}
