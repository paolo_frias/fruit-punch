﻿using UnityEngine;
using System.Collections;

public class Explosive : EnemyCollisionDestruction {

    public GameObject explosionSpriteObject;

    public override void die()
    {
        Instantiate(explosionSpriteObject, transform.position, transform.rotation);       // Make a deathsprite at this enemy's position when it dies.
        Destroy(gameObject);            // Destroy the fruit
        //gameController.AddScore(scoreValue);
    }
}
