﻿using UnityEngine;
using System.Collections;

public class DestroyByTime : MonoBehaviour
{
    public float lifeTime;
    void Start()
    {
        //destroy the gameobject lifeTime seconds after it was instantiated
        Destroy(gameObject, lifeTime);
    }
}
