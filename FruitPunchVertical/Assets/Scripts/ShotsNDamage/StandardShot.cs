﻿using UnityEngine;
using System.Collections;

public class StandardShot : ShotMover
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Enemy")
        {
            Destroy(gameObject);
        }
    }
}
