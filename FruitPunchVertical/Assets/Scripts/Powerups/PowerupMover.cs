﻿using UnityEngine;
using System.Collections;

public class PowerupMover : MonoBehaviour
{
    private Rigidbody2D powerUpBody;

    public float moveSpeed;

    public ScreenEdge theEdges;            // Edges of the screen

    protected PlayerController thePlayer;


    // Use this for initialization
    void Start ()
    {
        int[] initialSpeed = { -1, 1 };
        powerUpBody = GetComponent<Rigidbody2D>();
        powerUpBody.velocity = new Vector3(initialSpeed[Random.Range(0, initialSpeed.Length)], -1, 0) * moveSpeed;

        // Search for an instance of a Game Object tagged "Player"
        GameObject playerObject = GameObject.FindWithTag("Player");


        // if we found one...
        if (playerObject != null)
        {
            // assign our PlayerController handle to the instance that we found
            thePlayer = playerObject.GetComponent<PlayerController>();
        }

        // Error checking
        if (thePlayer == null)
        {
            Debug.Log("Cannot find 'Player' in this game!");
        }
    }



    // called once every "Physics step"
    void FixedUpdate()
    {
        if (powerUpBody.position.x < theEdges.xMin || powerUpBody.position.x > theEdges.xMax)
        {
            Vector3 temp = powerUpBody.velocity;
            temp.x = -temp.x;
            powerUpBody.velocity = temp;
        }
    }
}
