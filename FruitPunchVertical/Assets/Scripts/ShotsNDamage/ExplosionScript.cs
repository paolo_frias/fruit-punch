﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof (Collider2D))]
[RequireComponent(typeof (SpriteRenderer))]
public class ExplosionScript : DamageDealer
{

    public List<Sprite> spriteList = new List<Sprite>();
    Collider2D expColl;
    public float growthRate;

	// Use this for initialization
	void Start ()
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = spriteList[0];
        expColl = gameObject.GetComponent<Collider2D>();
        //Debug.Log(transform.localScale);
	}
	
	// Update is called once per frame
	void Update ()
    {
        /*transform.localScale += new Vector3(growthRate, growthRate, 0);
        if(transform.localScale.x >= 2 || transform.localScale.y >= 2)
        {
            Destroy(gameObject);
        }*/
	}
}
