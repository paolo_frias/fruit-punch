﻿using UnityEngine;
using System.Collections;

public class TeamLogo : GameController
{

    // Use this for initialization
    new void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        // Poll for keypress of Space
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // Load the next level
            UnityEngine.SceneManagement.SceneManager.LoadScene("TitleScreen");
        }
    }
}
