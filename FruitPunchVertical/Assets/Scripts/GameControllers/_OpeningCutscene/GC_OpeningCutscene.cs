﻿using UnityEngine;
using System.Collections;

public class GC_OpeningCutscene : GameController
{

	// Use this for initialization
	new void Start ()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        // Skip the cutscene if they hit esc
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("LevelSelect");
        }
    }
}
