﻿using UnityEngine;
using System.Collections;

public class EnemyCollisionDestruction : MonoBehaviour
{

    public GameObject deathSprite;          // Create when this enemy dies
    public int scoreValue;                  // The amount of points this guy is worth

    public int hitPoints;                   // Enemy HP
	public int damageDealt;

    private GameController gameController;  // handle to the GameController in Unity


	// Use this for initialization
	void Start ()
    {
        // Search for an instance of a Game Object tagged "GameController"
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");

        // if we found one...
        if (gameControllerObject != null)
        {
            // assign our GameController handle to the instance that we found
            gameController = gameControllerObject.GetComponent<GameController>();
        }

	}


    // called when "other" collides with this object. "other" is
    void OnTriggerEnter2D(Collider2D other)
    {

        // If "other" is the boundary, just do nothing
        if (other.tag == "Boundary")
        {
            return;
        }

        if (other.tag == "DamageDealer")
        {
            int dmg = other.gameObject.GetComponent<DamageDealer>().damageDealt;
            //Debug.Log("HIT: " + gameObject + " Damage Taken: " + dmg);
            takeDamage(dmg);
        }

		if(other.tag == "FruitBat") {
			Debug.Log("damage absorbed");
			Destroy(other.gameObject);
			takeDamage(hitPoints); //when hit by bat, just die
		}

    }

    void takeDamage(int dmg)
    {
        hitPoints -= dmg;
        if(hitPoints <= 0) {
            die();
        }
    }

    public virtual void die()
    {
        Instantiate(deathSprite, transform.position, transform.rotation);       // Make a deathsprite at this enemy's position when it dies.
        Destroy(gameObject);
        gameController.AddScore(scoreValue);
    }
}
