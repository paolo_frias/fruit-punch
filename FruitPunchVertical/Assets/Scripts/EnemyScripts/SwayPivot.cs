﻿using UnityEngine;
using System.Collections;

public class SwayPivot : MonoBehaviour
{
    public float rotationDegrees;                               // The amount of degrees to sway between
    public float rotationPeriod;                                // The time to spend rotating

    private float timeElapsed;

	// Update is called once per frame
	void Update ()
    {
        timeElapsed = timeElapsed + Time.deltaTime;                 // update time elapsed for rotation
        float interval = Mathf.Sin(timeElapsed / rotationPeriod);   // Sine function for smooth transition between -1 and 1
        transform.localRotation = Quaternion.Euler(new Vector3(0, 0, interval * rotationDegrees));
	}
}
